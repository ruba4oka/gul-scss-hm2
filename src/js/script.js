
"use strict";

const navigationBtnMenu = document.getElementById('navigation-btn-menu');
const navigationBtnExit = document.getElementById('navigation-btn-exit');
const navigationList = document.getElementById('header__navigation-list');

const showMenu = false;

const openMenu = () => {
    console.log('openMenu')
    navigationBtnExit.classList.replace('hide-element','show-element');
    navigationBtnMenu.classList.replace('show-element','hide-element');
    navigationList.classList.add('show-element');
}


const closeMenu = () => {
    console.log('closeMenu')
    navigationBtnMenu.classList.replace('hide-element','show-element');
    navigationBtnExit.classList.replace('show-element','hide-element');
    navigationList.classList.replace('show-element','hide-element');
}

navigationBtnMenu.addEventListener('click', openMenu);
navigationBtnExit.addEventListener('click', closeMenu);

