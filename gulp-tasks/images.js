const {src, dest} = require("gulp");
const {bs} = require("./serv.js");
const imagemin = require('gulp-imagemin');


function images() {
    return src("./src/images/**/*.{jpg,jpeg,png,gif,tiff,svg}")
        .pipe(imagemin())
        .pipe(dest("./dist/img"))
        .on("end", bs.reload);
}


function images_copy() {
    return src("./src/images/**/*.{jpg,jpeg,png,gif,tiff,svg}")
        .pipe(dest("./dist/img"))
        .on("end", bs.reload);
}

exports.images = images;
exports.images_copy = images_copy;
