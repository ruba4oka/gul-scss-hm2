const {watch, series, parallel} = require("gulp");
const {scripts} = require("./scripts.js");
const {styles} = require("./styles.js");
const {images} = require("./images.js");
const {bs} = require("./serv.js");

function bsReload() {
    return bs.reload();
}

function watcher() {
    watch("*.html").on("change", bsReload);
    watch("./src/js/*.js").on("change", series(scripts, bsReload));
    watch("./src/styles/*.scss").on("change", series(styles, bsReload));

    watch("./src/images/**/*.{jpg,jpeg,png,gif,tiff,svg}").on(
        "change",
        parallel(images)
    );
}

exports.watcher = watcher;

