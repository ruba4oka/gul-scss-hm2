const {src, dest} = require("gulp");
const {bs} = require("./serv.js");
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require("gulp-sourcemaps");
const concat = require('gulp-concat');
const minifyCSS = require('gulp-minify-css');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

const AUTOPREFIXER = [
    '> 1%',
    'ie >= 8',
    'edge >= 15',
    'ie_mob >= 10',
    'ff >= 45',
    'chrome >= 45',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4',
    'bb >= 10'
];

function styles() {
    return src(["./node_modules/normalize.css/normalize.css","./src/styles/style.scss"])
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: "expanded"}).on("error", sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: AUTOPREFIXER
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(sourcemaps.write())
        .pipe(minifyCSS())
        .pipe(concat('style.min.css'))
        .pipe(dest("./dist/css"))
        .pipe(bs.reload({stream: true}));
}

exports.styles = styles;
