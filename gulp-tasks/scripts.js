const {src, dest} = require("gulp");
const {bs} = require("./serv.js");
const minify = require('gulp-minify');
const rename = require("gulp-rename");
const uglify = require('gulp-uglify');

const srcList = ["./src/js/script.js"]

function scripts() {
    return src(srcList)
        .pipe(minify())
        .pipe(uglify())
        .pipe(rename('scripts.min.js'))
        .pipe(dest("./dist/js"))
        .pipe(bs.reload({stream: true}));
}

exports.scripts = scripts;
